'use strict';
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = mongoose.model('User');

exports.signUp = (req,res) => {
    // check if username contain whitespace
    if(req.body.userName.trim() === '' || req.body.userName.includes(' ')){
        return res.json({
            message: "Username can not be null or contain white-space!"
        });
    // check if password contain whitespace
    } else if (req.body.password.trim() === '' || req.body.password.includes(' ')){
        return res.json({
            message: "Password can not be null or contain white-space!"
        });
    // check if fullname is null
    } else if (req.body.fullName.trim() === ''){
        return res.json({
            message: "Full-name can not be null!"
        });
    } else {
        // create User data
        let newUser = new User();
        newUser.userName = req.body.userName.replace(/\s/g,'');
        const reqPassword = req.body.password.replace(/\s/g,'');
        newUser.encryptedPassword = bcrypt.hashSync(reqPassword, 10);
        newUser.fullName = req.body.fullName.trim();
        newUser.isActivated = false;
        newUser.save((err,user) => {
            if(err) {
                // duplicate username
                return res.json({
                    message: "This username is already in use! Please use another username!"
                });
            } else {
                // return data
                return res.json({
                    message: 'Sign up successfully.',
                    fullName: user.fullName
                });
            }
        })
    }
};

exports.signIn = (req, res) => {
    let {userName, password} = req.body;
    //search user in db
    User.findOne({
        userName: userName
    }, (err, user) => {
    //error
        if (err) throw err;
        //user not found
        if (!user) {
            res.json({ message: 'Authentication failed. User not found.' });
            //user found
        } else if (user) {
            // wrong password
            if (!bcrypt.compareSync(password, user.encryptedPassword)) {
                res.json({ message: 'Authentication failed. Wrong password.' });
            // Not yet activated
            } else if (!user.isActivated) {
                res.json({message: 'Authentication failed. Account is not activated yet. '});
            // allow login + create token
            } else {
                // create token
                let token = jwt.sign({
                        userName: user.userName,
                        fullName: user.fullName
                    }, 'RESTFULAPIs' );
                // save session
                req.session.userName = user.userName;
                req.session.fullName = user.fullName;
                req.session.token = token;
                // return data
                return res.json({
                    message: 'Successfully logged in.',
                    fullName: user.fullName,
                    token: token
                });
            }
        }
    });
};

exports.checkAuth = (req,res) => {
    // check if session already exist or not
    (req.session.fullName) ? (
        res.json({
            message: 'Already logged in.',
            fullName: req.session.fullName,
            token: req.session.token
        })
    ) : (
        res.json({
            message: 'Session ended or does not exist. Please login again!'
        })
    )
}

exports.signOut = (req,res) => {
    req.session.userName = undefined;
    req.session.fullName = undefined;
    req.session.token = undefined;
    res.json({message: 'Logged out.'})
}