'use strict';
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Bookmark = mongoose.model('Bookmark');

exports.addBookmark = (req,res) => {
        if( req.body.token === req.session.token ) {
            // create new bookmark
            let newBookmark = new Bookmark();
            newBookmark.bookmarkName = req.body.bookmarkName;
            newBookmark.bookmarkLink = req.body.bookmarkLink;
            // req.session.userName
            newBookmark.Owner = 'abc';
            // save
            newBookmark.save((err, bookmark) => {
                err ? res.json({message: 'An error occurred when add new bookmark!'}) : res.json({message: 'Add new bookmark successfully!'});
            })
        }
}

exports.getBookmark = async(req,res) => {
    ( req.body.token === req.session.token ) ? (
        Bookmark.find({Owner: req.session.userName}).lean().exec((err, bookmark) => {
            err ? (
                res.json({message: 'An error occurred!'})
            ) : (
                res.json({
                    message: 'Get bookmark successfully!',
                    bookmarks: bookmark
                })
            )
        })
    ) : (
        res.json({message: 'Invalid session!'})
    )
}