'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookmarkSchema = new Schema ({
    bookmarkName: {
        type: String,
        required: true
    },
    bookmarkLink: {
        type: String,
        required: true
    },
    Owner: {
        type: String,
        ref: 'User',
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
})

bookmarkSchema.virtual('createdBy', {
    ref: 'User',
    localField: 'Owner',
    foreignField: 'userName',
    justOne: false
});

bookmarkSchema.set('toJSON', {virtuals: true});

mongoose.model('Bookmark', bookmarkSchema);