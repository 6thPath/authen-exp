'use strict';
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const userSchema = new Schema ({
    userName: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true
    },
    encryptedPassword: {
        type: String,
        required: true
    },
    fullName: {
        type: String,
        trim: true,
        required: true
    },
    isActivated: {
        type: Boolean,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

userSchema.methods.comparePassword = (password) => {
    return bcrypt.compareSync(password, this.encryptedPassword);
};

mongoose.model('User', userSchema);