'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./api/models/userModel.js');
const Bookmark = require('./api/models/bookmarkModels.js');
const session = require('express-session');
const jsonwebtoken = require('jsonwebtoken');

const app = express();

// jwt
app.use((req, res, next) => {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
    jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function(err, decode) {
        if (err) req.user = undefined;
        req.user = decode;
        next();
    });
    } else {
    req.user = undefined;
    next();
    }
});

var port = process.env.PORT || 4000;
app.listen(port, () => {console.log(`Server started at port ${port}`)});

// database
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://Paths:fnc2811@ds147420.mlab.com:47420/fncdb',() => console.log('Database connected!'));
const db = mongoose.connection;
db.on('error', () => console.log('MongoDB connection error!'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// static field
app.use(express.static('./public'));


// express session
app.use(session({
    secret: 'hadesencrypted',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 1000*60*60 }
}));

// view engine for test
// app.set('view engine', 'ejs');
// app.set('views', './views');

// test routes
app.get('/', (req,res) => {
    res.render('index');
})

// ============================================================================= //
const userHandlers = require('./api/controllers/userController.js');

// signup new account
app.route('/auth/signUp')
    .post(userHandlers.signUp);

// login
app.route('/auth/signIn')
    .post(userHandlers.signIn);

// check if session ended or not
app.route('/auth/checkAuth')
    .get(userHandlers.checkAuth);

// logout
app.route('/auth/signOut')
    .get(userHandlers.signOut);


// ============================================================================= //
const bookmarkHandlers = require('./api/controllers/bookmarkController.js');

// post bookmark
app.route('/bookmark/postBookmark')
    .post(bookmarkHandlers.addBookmark);

// get bookmark
app.route('/bookmark/getBookmark')
    .post(bookmarkHandlers.getBookmark);

module.exports =  app;