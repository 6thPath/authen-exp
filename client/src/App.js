import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, Switch } from 'react-router-dom';
import axios from 'axios';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import logo from './public/logo.svg';
import './public/App.css';
import Login from './components/Login';
import Register from './components/Register';
import Public from './components/Public';
import Protected from './components/Protected';
import AuthButton from './components/AuthButton';

export default class App extends Component {
    constructor(props){
    super(props);
    this.state = {
        loading: false,
        isLoggedIn: false,
        user: '',
        token: '',
        bookmark: []
    };
    this.signUp = this.signUp.bind(this);
    this.signIn = this.signIn.bind(this);
    this.signOut = this.signOut.bind(this);
    this.getBookmark = this.getBookmark.bind(this);
}

    signUp = (usr, psw, fn) => {
        this.setState({ loading: true });
        axios.post('/auth/signUp', {userName: usr, password: psw, fullName: fn})
        .then(res => res.data.message === 'Sign up successfully.' ?
            (
                this.setState({
                    loading: false,
                    // isLoggedIn: true,
                    // user: res.data.fullName,
                    // token: res.data.token
                }),
                this.successNotify(res.data.message + ' Welcome ' + res.data.fullName + '!')
            ) : (
                this.setState({loading: false}),
                this.errorNotify(res.data.message)
            )
        )
        .catch(err => console.log(err))
    }

    signIn = (usr, psw) => {
        this.setState({ loading: true });
        axios.post('/auth/signIn', {userName: usr, password: psw})
        .then(res => res.data.message === 'Successfully logged in.' ?
            (
                this.setState({
                    loading: false,
                    isLoggedIn: true,
                    user: res.data.fullName,
                    token: res.data.token
                }),
                this.successNotify(res.data.message)
            )
        :
            (
                this.setState({ loading: false }),
                this.errorNotify(res.data.message)
            )
        )
        .catch(err => console.log(err));
    }

    signOut = () => {
        this.setState({ loading: true });
        axios.get('/auth/signOut')
        .then(res =>
            this.successNotify(res.data.message),
            this.setState({
                loading: false,
                isLoggedIn: false,
                user: '',
                token: '',
                bookmark: []
            })
        )
        .catch(err => console.log(err));
    }

    getBookmark = () => {
        // this.setState({ loading: true });
        axios.post('/bookmark/getBookmark', {token: this.state.token})
        .then(res => res.data.message === 'Get bookmark successfully!' ?
            (
                this.successNotify(res.data.message),
                this.setState({
                    loading: false,
                    bookmark: res.data.bookmarks
                })
            ) : (
                this.errorNotify(res.data.message),
                this.setState({
                    loading: false,
                    bookmark: []
                })
            )
        )
        .catch(err => console.log(err));
    }

    //Check authen whenever access to login page
    componentDidMount() {
        this.setState({ loading: true });
        axios.get('/auth/checkAuth')
        .then(res => res.data.message === 'Already logged in.' ?
                (
                    this.setState({
                        loading: false,
                        isLoggedIn: true,
                        user: res.data.fullName,
                        token: res.data.token
                    }),
                    this.successNotify(res.data.message + ' Redirecting...')
                ) : (
                    this.setState({loading: false}),
                    this.errorNotify(res.data.message)
                )
        )
        .catch(err => console.log(err))
    }

    //Error notify
    errorNotify = (message) => {
        toast.error(message, {
            position: "bottom-center",
            autoClose: 2000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: false,
            draggablePercent: true,
            className: 'rnotify'
        })
    }

    //Success notify
    successNotify = (message) => {
        toast.success(message, {
            position: "bottom-center",
            autoClose: 2000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: false,
            draggablePercent: true,
            className: 'rnotify'
        })
    }

    render() {
        let { loading } = this.state;
        if( loading ) {
            //loading animation
            return <div className="loadingContainer"><img src={logo} className="loadingAnimation" alt="Loading..."/></div>
        }
        return (
        <Router>
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="AppLogo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <AuthButton auth={this.state.isLoggedIn} userName={this.state.user} onSignOut={this.signOut} />
                <hr/>
                <ul className='navBar'>
                    <li><Link to='/public' >Public</Link></li>
                    <li><Link to='/protected' >Protected</Link></li>
                </ul>
                <hr/>
                <div className='mainContainer'>
                    <Switch>
                        <Route path='/register' render={() => !this.state.isLoggedIn ?
                            <Register onSignUp={this.signUp} /> : <Redirect to='/public' /> }
                        />
                        <Route path='/login' render={() => !this.state.isLoggedIn ?
                            <Login onSignIn={this.signIn} /> : <Redirect to='/public' /> }
                        />
                        <Route path='/public' component={Public}/>
                        <Route path="/protected" render={() => this.state.isLoggedIn ?
                            (
                                <Protected getBookmark={this.getBookmark} bookmark={this.state.bookmark} />
                            ) : (
                                <Redirect to='/login' />
                            )}
                        />
                    </Switch>
                </div>
                <ToastContainer />
            </div>
        </Router>
        );
    }
}