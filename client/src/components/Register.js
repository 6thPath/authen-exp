import React, { Component } from 'react';

export default class Register extends Component {

    handleSignUp() {
        let username = this.refs.username.value;
        let password = this.refs.password.value;
        let fullname = this.refs.fullname.value;
        this.props.onSignUp(username, password, fullname);
    }
    render() {
        return (
            <div>
                <p>Register new account</p>
                <input ref='username' type='text' />{' '}
                <input ref='password' type='password' />{' '}
                <input ref='fullname' type='text' />{' '}
                <button onClick={this.handleSignUp.bind(this)}>Sign Up</button>
            </div>
        );
    }
}