import React, {Component} from 'react';

export default class Login extends Component {

    handleSignIn() {
        let username = this.refs.username.value;
        let password = this.refs.password.value;
        this.props.onSignIn(username, password);
    }
    render() {
        return (
            <div className='loginContainer'>
                <p>You must Sign-in to view the Protected page </p>
                <input ref='username' className='loginInput' type='text' />{' '}
                <input ref='password' className='loginInput' type='password' />{' '}
                <button className='btnLogin' onClick={this.handleSignIn.bind(this)}>Log in</button>
            </div>
        );
    }
}