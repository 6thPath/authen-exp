import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import 'https://use.fontawesome.com/releases/v5.2.0/css/all.css'

export default class AuthButon extends Component {

    handleSignOut() {
        this.props.onSignOut();
    }

    render() {
        return (
            <div className='authenButton'>
                {
                    this.props.auth && this.props.userName !== '' ?
                        <p>Welcome {this.props.userName}!{" "}
                            <button onClick={this.handleSignOut.bind(this)}>Sign out</button>
                        </p>
                    :
                        <div>
                            <p>You are not logged in.</p>
                            <NavLink to='/login'>Sign in</NavLink>
                            <i class="fas fa-address-book"></i>
                            <NavLink to='/register'>Sign up</NavLink>
                        </div>
                }
            </div>
        );
    }
}