import React, {Component} from 'react';

export default class Protected extends Component {
    componentDidMount() {
        this.props.getBookmark();
    }

    render() {
        return (
            <div>
                {
                    this.props.bookmark.map((bm,i) => (
                    <div>
                        <p key={i}>{bm.bookmarkName} <span><a href={bm.bookmarkLink} target='_blank'>Navigate to this bookmark</a></span></p>
                        <br/>
                    </div>
                    ))
                }
            </div>
        );
    }
}